#include <Servo.h>

Servo miServo;
int angulo = 90;

void setup(){
 miServo.attach(9);
 Serial.begin(9600);
}

void loop(){
 unsigned char comando=0;
 if(Serial.available()){
  comando=Serial.read();
  
  if(comando=='a')angulo+=10;
  else if(comando=='z')angulo-=10;
  angulo=constrain(angulo,0,180);  
 }
 miServo.write(angulo);
 Serial.print("Angulo:" + angulo);
 delay(100);
}
